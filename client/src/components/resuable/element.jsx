import React from "react";

import {
  TextElement,
  RadioElement,
  SelectElement,
  CheckBoxElement,
} from "./utils";
const Element = (props) => {
  const {data} = props;

  return (
    <div>
      <input
        type="checkbox"
        checked={props.data.selected === 1 ? true : false}
        onChange={props.onChangecheck}
      />
      <label>{props.data.displayName}</label>

      {props.data.inputType === "text" ? (
        <TextElement
          name={data.displayName}
          value={data.value}
          disabled={data.selected !== 1 ? true : false}
          onChange={props.handleValueChange}
        />
      ) : null}

      {props.data.inputType === "select" ? (
        <SelectElement
          name={data.displayName}
          options={data.options}
          selectedOption={data.selectedOption}
          disabled={data.selected !== 1 ? true : false}
          onChange={props.handleValueChange}
        />
      ) : null}
      {props.data.inputType === "radio" ? (
        <RadioElement
          name={data.displayName}
          options={data.options}
          selectedOption={data.selectedOption}
          disabled={data.selected !== 1 ? true : false}
          onChange={props.handleValueChange}
        />
      ) : null}
      {props.data.inputType === "checkbox" ? (
        <CheckBoxElement
          name={data.displayName}
          options={data.options}
          selectedOption={data.selectedOption}
          disabled={data.selected !== 1 ? true : false}
          onChange={props.handleValueChange}
        />
      ) : null}
    </div>
  );
};

export default Element;
