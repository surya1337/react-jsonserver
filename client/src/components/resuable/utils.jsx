import React from "react";
export const TextElement = (props) => {
  return (
    <div>
      <input
        name={props.name}
        type="text"
        value={props.value}
        disabled={props.disabled}
        onChange={props.onChange}
      />
    </div>
  );
};
export const RadioElement = (props) => {
  return (
    <div>
      {props.options.map((option) => {
        return (
          <div key={option.key}>
            <input
              type="radio"
              id={option.key}
              name={props.name}
              value={option.key}
              disabled={props.disabled}
              checked={props.selectedOption === option.key}
              onChange={props.onChange}
            />
            <label htmlFor={option.key}>{option.value}</label>
            <br></br>
          </div>
        );
      })}
    </div>
  );
};
export const SelectElement = (props) => {
  return (
    <select
      name={props.name}
      onChange={props.onChange}
      disabled={props.disabled}
    >
      {props.options.map((option) => {
        return (
          <option
            key={option.key}
            value={option.key}
            selected={props.selectedOption === option.key}
          >
            {option.value}
          </option>
        );
      })}
    </select>
  );
};
export const CheckBoxElement = (props) => {
  return (
    <div>
      {props.options.map((option) => {
        return (
          <div key={option.key}>
            <input
              type="checkbox"
              id={option.key}
              name={props.name}
              value={option.key}
              disabled={props.disabled}
              checked={props.selectedOption.includes(option.key)}
              onChange={props.onChange}
            />
            <label htmlFor={option.key}>{option.value}</label>
          </div>
        );
      })}
    </div>
  );
};
