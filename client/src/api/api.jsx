import axios from "axios";
export const getData = async (options) => {
  try {
    const res = await axios(options);
    return res;
  } catch (error) {
    return error
  }
};
