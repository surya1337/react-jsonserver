import React, {Component} from "react";
import {getData} from "./api/api";
import Element from "./components/resuable/element";
class App extends Component {
  state = {
    names: [],
    personDetails: {},
  };
  componentDidMount() {
    this.getService();
  }
  getService = async () => {
    const options = {
      method: "GET",
      url: "http://localhost:3001/names",
    };
    const response = await getData(options);
    this.setState({names: response.data});
  };
  handleChange = (e) => {
    const id = e.target.value;
    if (+id) {
      this.setState({selectedID: id});
      this.getDetails(id);
    } else alert("Select valid Name");
  };
  getDetails = async (id) => {
    const options = {
      method: "GET",
      url: `http://localhost:3001/details/${+id}`,
    };
    const res = await getData(options);
    res.data.data.forEach((item) => {
      item.selected = 0;
    });
    this.setState({personDetails: res.data});
  };

  handlecheckBox = (item, e) => {
    const tempArray = {...this.state.personDetails};
    const temp = tempArray.data.filter((el) => {
      return el.displayName === item.displayName;
    });
    temp[0].selected = e.target.checked ? 1 : 0;
    this.setState({...this.state.personDetails, personDetails: tempArray});
  };

  handleValueChange = (item, e) => {
    const tempArray = {...this.state.personDetails};
    const temp = tempArray.data.filter((el) => {
      return el.displayName === item.displayName;
    });
    if (item.inputType === "text") {
      temp[0].value = e.target.value;
    } else if (item.inputType === "select") {
      temp[0].selectedOption = +e.target.value;
    } else if (item.inputType === "radio") {
      temp[0].selectedOption = +e.target.value;
    } else if (item.inputType === "checkbox") {
      if (e.target.checked) {
        temp[0].selectedOption.push(+e.target.value);
      } else {
        temp[0].selectedOption = temp[0].selectedOption.filter(
          (name) => name !== +e.target.value
        );
      }
    }
    this.setState({...this.state.personDetails, personDetails: tempArray});
  };
  handleSubmit = async (e) => {
    e.preventDefault();
    const options = {
      method: "PUT",
      url: `http://localhost:3001/details/${+this.state.selectedID}`,
      data: this.state.personDetails,
    };
    const res = await getData(options);
    console.log(res);
  };
  render() {
    const {names, personDetails} = this.state;
    return (
      <div>
        <form>
          <select
            name="selectedName"
            className="form-control"
            onChange={this.handleChange}
          >
            <option value="0">Select a Name</option>
            console.log(names)
            {names.length
              ? names.map((name) => (
                  <option key={name.id} value={name.id}>
                    {name.type}
                  </option>
                ))
              : null}
          </select>
          <br />
          {personDetails.id
            ? personDetails.data.map((item, index) => {
                return (
                  <div key={index}>
                    {/* lhs */}
                    <Element
                      data={item}
                      onChangecheck={(e) => this.handlecheckBox(item, e)}
                      handleValueChange={(e) => this.handleValueChange(item, e)}
                    />
                    {/* rhs */}
                  </div>
                );
              })
            : null}
          <button onClick={this.handleSubmit}>Submit</button>
        </form>
      </div>
    );
  }
}

export default App;
